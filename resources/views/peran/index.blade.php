@extends('layout.master')

@section('judul')
Halaman Peran
@endsection

@section('judul1')
Peran
@endsection

@section('content')
<a href="/peran/create" class="btn btn-success mb-3">Tambah Data</a>
    <div class="row">
        @forelse ($peran as $item)
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h3>{{$item->film_id}}</h3>
                        <p class="card-text">{{Str::limit($item->nama), 30}}</p>
                        <form action="/peran/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/peran/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/peran/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </div>
                </div>
            </div>
        @empty
            <h4>Data Peran Belum Ada</h4>
        @endforelse
    </div>
@endsection