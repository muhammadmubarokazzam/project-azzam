@extends('layout.master')

@section('judul')
<h1>Halaman Edit Peran {{$peran ->nama}}</h1>
@endsection

@section('content')

<form action="/peran/{{$peran ->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Peran</label>
      <input type="text" name= "nama" value="{{$peran ->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Film</label>
      <select name="film" class="form-control" id="" cols="30" rows="10">{{$peran ->film}}
        <option value="">---Pilih Film---</option>
        @foreach ($film as $item)
          @if ($item->id === $peran->film_id)
              <option value="{{$item->id}}" selected>{{$item->nama}}</option>
          @else
              <option value="{{$item->id}}">{{$item->nama}}</option>
          @endif
        @endforeach
      </select>
    </div>
    @error('film')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Cast</label>
        <select name="cast" class="form-control" id="" cols="30" rows="10">{{$peran ->cast}}
          <option value="">---Pilih Cast---</option>
          @foreach ($cast as $item)
            @if ($item->id === $peran->cast_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
      </div>
      @error('cast')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>

</form>

@endsection