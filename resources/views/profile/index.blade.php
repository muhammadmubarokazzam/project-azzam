@extends('layout.master')

@section('judul')
<h1>Halaman List profile</h1>
@endsection

@section('judul1')
<h1>profile</h1>
@endsection

@section('content')


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">id</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Alamat</th>
        <th scope="col">User</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($profile as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->umur}}</td>

                <td>
                    {{$item->bio}}
                </td>
                <td>
                  {{$item->alamat}}
                </td>
                <td>
                  {{$item->users_id}}
                </td>

            </tr>
        @empty
            <tr>
                <td>data masih kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection