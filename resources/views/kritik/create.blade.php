@extends('layout.master')

@section('judul')
Kritik
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>

// In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>
@endpush

@section('content')

<form action="/kritik" method="POST">
    @csrf

    <div class="form-group">
        <label>User</label> <br>
        <select name="users_id" class="js-example-basic-single" style="width: 100% " id="">
          <option value="">---pilih User---</option>
  
          @foreach ($users as $item)
              <option value="{{$item->id}}">{{$item->name}}</option>
          @endforeach
        </select>
      </div>
      @error('users_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Film</label> <br>
        <select name="film_id" class="js-example-basic-single" style="width: 100% " id="">
          <option value="">---pilih Film---</option>
  
          @foreach ($film as $item)
              <option value="{{$item->id}}">{{$item->judul}}</option>
          @endforeach
        </select>
      </div>
      @error('film_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Kritik Film</label>
      <textarea name="isi" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('isi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Point Nilai 1-100 (harus angka)</label>
        <input type="integer" name= "point" class="form-control">
      </div>
      @error('point')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror



    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection