@extends('layout.master')

@section('judul')
Halaman Detail Kritik
@endsection
@section('judul1')
Nama Kritik {{$kritik ->users_id}}
@endsection

@section('content')

<h1>{{$kritik->users_id}}</h1>
<p>{{$kritik ->film_id}}</p>
<p>{{$kritik ->isi}}</p>
<p>{{$kritik ->point}}</p>

<a href="/kritik" class="btn btn-secondary">Kembali</a>
@endsection