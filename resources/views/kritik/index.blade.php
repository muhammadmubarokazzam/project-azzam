@extends('layout.master')

@section('judul')
Halaman Kritik
@endsection

@section('judul1')
Kritik
@endsection

@section('content')
@auth
    <a href="/kritik/create" class="btn btn-primary my-2">Tambah Data</a>
@endauth

    <div class="row">
        @forelse ($kritik as $item)
            <div class="col-4">
                <div class="card">
                
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <p class="card-text">{{Str::limit($item->isi), 30}}</p>
                        @auth
                        <form action="/kritik/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/kritik/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/kritik/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                        @endauth

                        @guest
                        <a href="/kritik/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        @endguest
                    </div>
                </div>
            </div>
        @empty
            <h4>Data Peran Belum Ada</h4>
        @endforelse
    </div>
@endsection