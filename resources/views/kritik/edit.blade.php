@extends('layout.master')

@section('judul')
<h1>Halaman Edit Kritik {{$kritik ->users_id}}</h1>
@endsection

@section('content')

<form action="/kritik/{{$kritik ->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>User</label>
        <select name="users_id" class="form-control" id="" cols="30" rows="10">{{$kritik ->users_id}}
          <option value="">---Pilih user---</option>
          @foreach ($users_id as $item)
            @if ($item->id === $kritik ->users_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
      </div>
      @error('users_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Film</label>
        <select name="film_id" class="form-control" id="" cols="30" rows="10">{{$kritik ->film_id}}
          <option value="">---Pilih film---</option>
          @foreach ($film_id as $item)
            @if ($item->id === $kritik ->film_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
      </div>
      @error('film_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Kritik</label>
        <textarea name="isi" class="form-control" id="" cols="30" rows="10">{{$kritik ->isi}}</textarea>
      </div>
      @error('isi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Point Nilai 1-100 (harus angka)</label>
        <input type="integer" name= "point" value="{{$kritik ->point}}" class="form-control">
      </div>
      @error('point')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection