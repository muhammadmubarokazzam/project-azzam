@extends('layout.master')

@section('judul')
Halaman Detail Film {{$film ->judul}}
@endsection
@section('judul1')
Nama Film {{$film ->judul}}
@endsection

@section('content')

<img src="{{asset('gambar/'.$film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<p>{{$film ->ringkasan}}</p>
<p>{{$film ->tahun}}</p>
<p>{{$film ->poster}}</p>
<p>{{$film ->genre_id}}</p>

<a href="/film" class="btn btn-secondary">Kembali</a>
@endsection