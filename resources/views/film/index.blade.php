@extends('layout.master')

@section('judul')
Halaman List Film
@endsection

@section('judul1')
Film
@endsection

@section('content')
@auth
    <a href="/film/create" class="btn btn-primary my-2">Tambah Data</a>
@endauth

    <div class="row">
        @forelse ($film as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('gambar/'. $item->poster)}}" class="card-img-top" alt="...">
                
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <p class="card-text">{{Str::limit($item->ringkasan), 30}}</p>
                        @auth
                        <form action="/film/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                        @endauth

                        @guest
                        <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        @endguest
                    </div>
                </div>
            </div>
        @empty
            <h4>Data Peran Belum Ada</h4>
        @endforelse
    </div>
@endsection