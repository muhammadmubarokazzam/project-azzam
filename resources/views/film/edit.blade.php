@extends('layout.master')

@section('judul')
<h1>Halaman Edit Film {{$film ->judul}}</h1>
@endsection

@section('content')

<form action="/film/{{$film ->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Judul</label>
      <input type="text" name= "judul" value="{{$film ->judul}}" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control" id="" cols="30" rows="10">{{$film ->ringkasan}}</textarea>
      </div>
      @error('ringkasan')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
        <label>Tahun (harus angka)</label>
        <input type="integer" name= "tahun" value="{{$film ->tahun}}" class="form-control">
      </div>
      @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
        <label>Poster</label>
        <input type="text" name= "poster" value="{{$film ->poster}}" class="form-control">
      </div>
      @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Genre</label>
      <select name="genre" class="form-control" id="" cols="30" rows="10">{{$film ->genre}}
        <option value="">---Pilih Genre---</option>
        @foreach ($genre as $item)
          @if ($item->id === $film->genre_id)
              <option value="{{$item->id}}" selected>{{$item->nama}}</option>
          @else
              <option value="{{$item->id}}">{{$item->nama}}</option>
          @endif
        @endforeach
      </select>
    </div>
    @error('genre')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection