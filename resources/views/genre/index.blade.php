@extends('layout.master')

@section('judul')
<h1>Halaman List Genre</h1>
@endsection

@section('judul1')
<h1>Genre</h1>
@endsection

@section('content')

<a href="/genre/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>

                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>data masih kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection