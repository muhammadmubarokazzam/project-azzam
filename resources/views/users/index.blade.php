@extends('layout.master')

@section('judul')
<h1>Halaman List User</h1>
@endsection

@section('judul1')
<h1>User</h1>
@endsection

@section('content')


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">id</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($users as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->name}}</td>

                <td>
                    {{$item->email}}
                </td>
            </tr>
        @empty
            <tr>
                <td>data masih kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection