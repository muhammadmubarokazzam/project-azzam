@extends('layout.master')

@section('judul')
User
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>

// In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>
@endpush

@section('content')

<form action="/users" method="POST">
    @csrf

      <div class="form-group">
        <label>Nama User</label>
        <input type="text" name= "name" class="form-control">
      </div>
      @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Nama Email</label>
        <input type="text" name= "email" class="form-control">
      </div>
      @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Password</label>
        <input type="text" name= "password" class="form-control">
      </div>
      @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection