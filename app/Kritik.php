<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table = 'kritik';
    protected $fillable = ['users_id','film_id','isi','point'];
}