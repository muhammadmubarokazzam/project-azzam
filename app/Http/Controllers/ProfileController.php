<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use File;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

        //$this->middleware('log')->only('index');

        //$this->middleware('subscribed')->except('store');
    }

    public function create(){

    }

    public function store(Request $request){

    }

    public function index(){
        $profile = DB::table('profile') ->get();
        return view('profile.index', compact('profile'));
    }

    public function show($id){
        $profile = Profile::findOrFail($id);
        return view('profile.show',compact('profile'));
    }

    public function edit($id){

    }

    public function update($id, Request $request){
        
    }

    public function destroy($id){

    }

}