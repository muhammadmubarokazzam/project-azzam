<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('file.register');
    }

    public function kirim(Request $request){
        $namasatu = $request['depan'];
        $namadua =  $request['belakang'];
        return view('file.welcome', compact('namasatu','namadua'));
    }
}
