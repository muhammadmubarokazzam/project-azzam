<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

        //$this->middleware('log')->only('index');

        //$this->middleware('subscribed')->except('store');
    }

    public function create(){
        $genre = DB::table('genre')->get();
        return view('film.create',compact('genre'));
    }

    public function store(Request $request){
        $request ->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);
        $gambarposter = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('gambar'), $gambarposter);
        

        DB::table('film') ->insert([
            'judul' => $request['judul'],
            'ringkasan' => $request['ringkasan'],
            'tahun' => $request['tahun'],
            'poster' => $gambarposter,
            'genre_id' => $request['genre_id'],
        ]);

        Alert::success('Berhasil !', 'Tambah Data Film Berhasil');
        return redirect('/film');
        


    }

    public function index(){
        $film = DB::table('film') ->get();
        return view('film.index', compact('film'));
    }

    public function show($id){
        $film = Film::findOrFail($id);
        return view('film.show',compact('film'));
    }

    public function edit($id){
        $genre = DB::table('genre')->get();
        return view('film.create',compact('genre'));

        $film = DB::table('film')->where('id', $id)->first();
        return view('film.edit',compact('film'));

        $film = Film::findOrFail($id);
        return view('film.edit',compact('film','genre'));
    }

    public function update($id, Request $request){
        $request ->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $film = Film::find($id);

        if($request ->has('poster')){
            $gambarposter = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('gambar'), $gambarposter);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->poster = $gambarposter;
            $film->genre_id = $request->genre_id;
        }
        else{
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
            }

        $film->update();
        Alert::success('Update','Data Film Berhasil Diupdate !');
        return redirect('/film');
        
    }

    public function destroy($id){
        $film = Film::find($id);

        $path = "gambar/";
        File::delete($path . $film->poster);
        $film->delete();
        Alert::success('Delete','Data Film Berhasil Dihapus !');
        return redirect('/film');
    }

}