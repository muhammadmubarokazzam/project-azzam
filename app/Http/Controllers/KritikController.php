<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kritik;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class KritikController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

        //$this->middleware('log')->only('index');

        //$this->middleware('subscribed')->except('store');
    }

    public function create(){
        $film = DB::table('film')->get();
        return view('kritik.create',compact('film'));

        $users = DB::table('users')->get();
        return view('kritik.create',compact('users'));
    }

    public function store(Request $request){
        $request ->validate([
            'users_id' => 'required',
            'film_id' => 'required',
            'isi' => 'required',
            'point' => 'required',
        ]);

        DB::table('kritik') ->insert([
            'users_id' => $request['users_id'],
            'film_id' => $request['film_id'],
            'isi' => $request['isi'],
            'point' => $request['point'],
        ]);

        Alert::success('Berhasil !', 'Tambah Data Kritik Berhasil');
        return redirect('/kritik');
        


    }

    public function index(){
        $kritik = DB::table('kritik') ->get();
        return view('kritik.index', compact('kritik'));
    }

    public function show($id){
        $kritik = Kritik::findOrFail($id);
        return view('kritik.show',compact('kritik'));
    }

    public function edit($id){
        $film = DB::table('film')->get();
        return view('kritik.create',compact('film'));

        $users = DB::table('users')->where('id', $id)->first();
        return view('kritik.create',compact('users'));

        $kritik = Kritik::findOrFail($id);
        return view('kritik.edit',compact('users','film'));
    }

    public function update($id, Request $request){
        $request ->validate([
            'users_id' => 'required',
            'film_id' => 'required',
            'isi' => 'required',
            'point' => 'required',
        ]);

        $kritik = Kritik::find($id);
        if($request ->has('kritik')){
            $kritik->users_id = $request->users_id;
            $kritik->film_id = $request->film_id;
            $kritik->isi = $request->isi;
            $kritik->point = $request->point;
        }

        else{
            $kritik->users_id = $request->users_id;
            $kritik->film_id = $request->film_id;
            $kritik->isi = $request->isi;
            $kritik->point = $request->point;
            }

        $kritik->update();
        Alert::success('Update','Data kritik Berhasil Diupdate !');
        return redirect('/kritik');
        
    }

    public function destroy($id){
        $kritik = Kritik::find($id);

        $Kritik->delete();
        Alert::success('Delete','Data Kritik Berhasil Dihapus !');
        return redirect('/kritik');
    }

}