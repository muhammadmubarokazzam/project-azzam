<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Peran;
use File;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = DB::table('peran') ->get();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = DB::table('film')->get();
        $cast = DB::table('cast')->get();
        return view('peran.create',compact('film','cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'film_id' => 'required', 
            'cast_id' => 'required',
            'nama' => 'required',
        ]);

        $peran = new peran;
        $peran->film_id = $request->film_id;
        $peran->cast_id = $request->cast_id;
        $peran->nama = $request->nama;

        $peran->save();
        return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::findOrFail($id);
        return view('peran.show',compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        return view('film.create',compact('genre'));

        $film = DB::table('film')->where('id', $id)->first();
        return view('film.edit',compact('film'));

        $film = Film::findOrFail($id);
        return view('peran.edit',compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request ->validate([
            'film_id' => 'required',
            'cast_id' => 'required',
            'nama' => 'required',
        ]);

        $peran = Peran::find($id);

        if($request ->has('poster')){

            $peran->film_id = $request->film_id;
            $peran->cast_id = $request->cast_id;
            $peran->nama = $request->nama;
        }
        else{
            $peran->film_id = $request->film_id;
            $peran->cast_id = $request->cast_id;
            $peran->nama = $request->nama;
            }

        $peran->update();

        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::find($id);

        $path = "gambar/";
        File::delete($path . $peran->poster);
        $peran->delete();

        return redirect('/peran');
    }
}
