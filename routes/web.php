<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@alamat');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/table', function(){
        return view('table.table');
}
);

route::get('/data-table', function(){
        return view('table.data-table');
}
);

Route::group(['middleware' => ['auth']], function (){
        //CRUD Cast

        Route::get('/cast/create','CastController@create');
        Route::post('/cast', 'CastController@store');
        Route::get('/cast', 'CastController@index');
        Route::get('/cast/{cast_id}', 'CastController@show');
        Route::get('/cast/{cast_id}/edit', 'CastController@edit');
        Route::put('/cast/{cast_id}', 'CastController@update');
        Route::delete('/cast/{cast_id}', 'CastController@destroy');
});





//CRUD Genre

Route::get('/genre/create','GenreController@create');
Route::post('/genre', 'GenreController@store');
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id}', 'GenreController@show');

Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
Route::put('/genre/{genre_id}', 'GenreController@update');

Route::delete('/genre/{genre_id}', 'GenreController@destroy');

//CRUD Film

Route::resource('film','FilmController');
Route::get('/film/create','FilmController@create');
Route::post('/film', 'FilmController@store');
Route::get('/film', 'FilmController@index');
Route::get('/film/{film_id}', 'FilmController@show');

Route::get('/film/{film_id}/edit', 'FilmController@edit');
Route::put('/film/{film_id}', 'FilmController@update');

Route::delete('/film/{film_id}', 'FilmController@destroy');

//CRUD Peran
Route::resource('peran','PeranController');

//CRUD Profile
Route::resource('profile','ProfileController');
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{profile_id}', 'ProfileController@show');



//CRUD Users
Route::resource('users','UserController');
Route::get('/users', 'UserController@index');
Route::get('/users/{users_id}', 'UserController@show');

//Unknown
Auth::routes();
